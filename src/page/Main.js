import React from 'react'
import Header from '../components/header/Header'
import NavBar from '../components/navbar/NavBar'
import Container from 'react-bootstrap/Container'
import { Col, Row } from 'react-bootstrap'
import DashboardNav from '../components/navbar/DashboardNav'
import Desctiption from '../components/userDetail/Desctiption'
import AccessDennyCards from '../components/card/AccessDennyCards'
import TopUsers from '../components/card/TopUsers'
import Tags from '../components/card/Tags'
import Properties from '../components/card/Properties'
import DataProfiling from '../components/userDetail/DataProfiling'
import DataQuality from '../components/userDetail/DataQuality'
import SampleColumn from '../components/userDetail/SampleColumn'
import SampleContent from '../components/userDetail/SampleContent'
import Dashboard from '../components/userDetail/Dashboard'

const Main = () => {
    return (
        <>
            <Header />
            <Container>
                <Row>
                    <Col sm="1">
                        <NavBar />
                    </Col>
                    <Col sm="11" style={{ marginRight: 0, paddingRight: 0 }}>
                        <Container>
                            <Dashboard />
                        </Container>
                        <DashboardNav />
                        <Row style={{marginTop: 10}}>
                            <Col sm="9">
                                <Desctiption />
                                <AccessDennyCards />
                                <DataProfiling />
                                <DataQuality />
                                <SampleColumn />
                                <SampleContent />
                            </Col>
                            <Col>
                                <TopUsers />
                                <Tags />
                                <Properties />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Main
