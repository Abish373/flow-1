import React from 'react'
import './header.css'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'


const Header = () => {

    return (
        <Container className="mt-10 shadow-sm pt-3 bg-white rounded sticky-top">
            <Row>
                <Col lg="1">
                    <h2>Logo</h2>
                </Col>
                <Col lg="7">
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2 header-search" />
                    </Form>
                </Col>
                <Col>
                    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around'}}>
                        <img className="icon" alt="" src={require('./assets/close.svg')} />
                        <img className="icon" alt="" src={require('./assets/close.svg')} />
                        <div style={{display: 'flex', width: 150, justifyContent: 'space-around'}}>
                            <img className="icon" alt="" src={require('./assets/close.svg')} />
                            <div>
                                <h4>John Dae</h4>
                                <p>Admin</p>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}

export default Header
