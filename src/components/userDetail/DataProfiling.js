import React from 'react'
import { Col, Row } from 'react-bootstrap'

const DataProfiling = () => {
    return (
        <div className="mt-5">
            <h5>Data Profiling</h5>
            <Row className="m-5">
                <Col>
                    <h1>Chart</h1>
                </Col>
                <Col>
                    <div className="pl-3" style={{ borderLeft: '1px solid grey' }}>
                        <h6>Empty value count</h6>
                        <h6>Max Length</h6>
                        <h6>Min Length</h6>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default DataProfiling
