import React from 'react'
import { Card, Col, Row } from 'react-bootstrap'

const AccessDennyCards = () => {
    return (
        <Row>
        <Col>
            <Card className="bg-light">
                <Card.Body>
                    <Card.Title>Acceess Request</Card.Title>
                    <Card.Text>
                       For acces please contact <Card.Link href="#">Paul</Card.Link>
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col>
            <Card className="bg-light">
                <Card.Body>
                    <Card.Title>Data Source</Card.Title>
                    <Card.Text>
                    Data type type form <Card.Link href="#">Data_type</Card.Link>
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        </Row>
    )
}

export default AccessDennyCards
