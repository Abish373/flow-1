import React from 'react'
import { Card } from 'react-bootstrap'

const Properties = () => {
    return (
            <Card className="mt-3 bg-light">
                <Card.Title>Properties</Card.Title>
                <Card.Body>
                    <div>
                        <p>created on</p>
                        <p>19 June 2019</p>
                    </div>
                    <div>
                        <p>created by</p>
                        <p>John Doe</p>
                    </div>
                </Card.Body>
            </Card>
    )
}

export default Properties
