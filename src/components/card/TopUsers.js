import React from 'react'
import Card from 'react-bootstrap/Card'

const TopUsers = () => {
    return (
            <Card  className="mt-3 bg-light">
                <Card.Body>
                    <Card.Title>Top Users</Card.Title>
                    <Card.Body>John</Card.Body>
                    <Card.Body>John</Card.Body>
                    <Card.Body>John</Card.Body>
                    <Card.Body>John</Card.Body>
                </Card.Body>
            </Card>
    )
}

export default TopUsers
