import React from 'react'
import { Card, Col, Row } from 'react-bootstrap'

const Tags = () => {
    return (
            <Card className="mt-3 bg-light">
                <Card.Title>Tags</Card.Title>
                <Card.Body>
                    <Row>
                        <Col>
                            <p style={{backgroundColor: 'lightblue', borderRadius: 10, textAlign: 'center', paddingHorizontal: 5}}>Tags</p>
                            <p style={{backgroundColor: 'lightblue', borderRadius: 10, textAlign: 'center', paddingHorizontal: 5}}>Tags</p>
                        </Col>
                        <Col>
                            <p style={{backgroundColor: 'lightblue', borderRadius: 10, textAlign: 'center', paddingHorizontal: 5}}>Tags</p>
                            <p style={{backgroundColor: 'lightblue', borderRadius: 10, textAlign: 'center', paddingHorizontal: 5}}>Tags</p>
                        </Col>
                        <Col>
                            <p style={{backgroundColor: 'lightblue', borderRadius: 10, textAlign: 'center', paddingHorizontal: 5}}>Tags</p>
                            <p style={{backgroundColor: 'lightblue', borderRadius: 10, textAlign: 'center', paddingHorizontal: 5}}>Tags</p>
                        </Col>
                    </Row>
                    
                </Card.Body>
            </Card>
    )
}

export default Tags
